package com.example.kafkademo.kafka;

import com.example.kafkademo.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducer {

    @Autowired
    private KafkaTemplate<Object, Object> kafkaTemplate;

    public void postMessage(UserDetails userDetails){
        kafkaTemplate.send("topic-1", userDetails);
    }

}
