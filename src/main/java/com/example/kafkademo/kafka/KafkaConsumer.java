package com.example.kafkademo.kafka;

import com.example.kafkademo.model.UserDetails;
import org.apache.catalina.User;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(groupId="consumerGroup-1", topics = "topic-1", containerFactory = "jsonKafkaListenerContainerFactory")
    public void listenEvent(ConsumerRecord<String, UserDetails> record, @Payload UserDetails payload){
        System.out.println("MessageReceived1: " + payload + " " + payload.toString());
    }
    @KafkaListener(groupId="consumerGroup-2", topics = "topic-1", containerFactory = "jsonKafkaListenerContainerFactory")
    public void listenEvent1(ConsumerRecord<String, UserDetails> record, @Payload UserDetails payload){
        System.out.println("MessageReceived2: " + payload + " " + payload.toString());
    }
}
