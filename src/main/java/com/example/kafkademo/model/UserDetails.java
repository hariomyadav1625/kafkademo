package com.example.kafkademo.model;

import lombok.Data;

@Data
public class UserDetails {
    private String name;
    private int age;
}
