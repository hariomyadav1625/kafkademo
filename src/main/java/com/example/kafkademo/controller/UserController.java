package com.example.kafkademo.controller;

import com.example.kafkademo.kafka.KafkaProducer;
import com.example.kafkademo.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    private KafkaProducer producer;
    @PostMapping("/user")
    public void postUserDetails(@RequestBody UserDetails userDetails){
        producer.postMessage(userDetails);
    }

}
